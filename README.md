# Test semantic release

[![pipeline status](https://gitlab.com/sis-hephaestus/qa-npm-skel/badges/main/pipeline.svg)](https://gitlab.com/sis-hephaestus/qa-npm-skel/-/commits/main)
[![coverage report](https://gitlab.com/sis-hephaestus/qa-npm-skel/badges/main/coverage.svg)](https://gitlab.com/sis-hephaestus/qa-npm-skel/-/commits/main)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
[![semantic-release: angular](https://img.shields.io/badge/semantic--release-angular-e10079?logo=semantic-release)](https://github.com/semantic-release/semantic-release)
[![made-with-javascript](https://img.shields.io/badge/Made%20with-JavaScript-1f425f.svg)](https://www.javascript.com)
[![Npm package version](https://badgen.net/npm/v/fastify)](https://npmjs.com/package/fastify)
[![GitHub license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)](https://github.com/Naereen/StrapDown.js/blob/master/LICENSE)
[![Latest Release](https://gitlab.com/sis-hephaestus/qa-npm-skel/-/badges/release.svg)](https://gitlab.com/sis-hephaestus/qa-npm-skel/-/releases)

