const { v4: uuid } = require('uuid')

const id = () => uuid()

const sayHi = (b) => {
  if (b) {
    return 'Hi'
  }
  return 'Bye'
}

module.exports = {
  id,
  sayHi
}
