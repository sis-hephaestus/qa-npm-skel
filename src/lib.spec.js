const { describe, it } = require('mocha')
const chai = require('chai')
const dirtyChai = require('dirty-chai')

const { lc } = require('./lib')

chai.use(dirtyChai)

const { expect } = chai

describe('should run', function () {
  it('should lowercase the string correctly', function () {
    const s = lc('This Is A Test')
    expect(s).not.to.undefined()
    expect(s).to.equals('this is a test')
  })
})
