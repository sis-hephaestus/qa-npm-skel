const { describe, it } = require('mocha')
const chai = require('chai')
const dirtyChai = require('dirty-chai')

const { id, sayHi } = require('./app')

chai.use(dirtyChai)

const { expect } = chai

describe('should run', function () {
  it('should generate a valid id', function () {
    const uuid = id()
    expect(uuid).not.to.undefined()
  })
})

describe('should speak', function () {
  it('should say hi', function () {
    const b = sayHi(true)
    expect(b).to.equals('Hi')
  })
  it('should say bye', function () {
    const b = sayHi(false)
    expect(b).to.equals('Bye')
  })
})
